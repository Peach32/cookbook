class Instruction extends HTMLElement {

  constructor() {
    super();

    // Create a shadow root
    const shadowRoot = this.attachShadow({mode: 'open'});

    // Apply external sytle to the shadow DOM
    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
    linkElem.setAttribute('href', 'instruction.css');
    shadowRoot.appendChild(linkElem);

    // Create the outer shell
    const wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'instruction');

    // Populate Text
    const text = document.createElement('p');
    text.innerHTML = this.innerHTML;
    wrapper.appendChild(text);

    // TODO: haccp
    // TODO: notes

    this.shadowRoot.appendChild(wrapper);
  }

  // TODO: Callbacks?
}

customElements.define("instruction-item", Instruction);
