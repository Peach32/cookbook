
#include <iostream>
#include "crow.h"
#include "crow/middlewares/cors.h"

int main() {
  std::cout << "Hello, World" << std::endl;

  // Declare Crow App
  crow::SimpleApp app;
  //crow::App<crow::CORSHandler> app;

  // Set CORS to global for now
//  auto& cors = app.get_middleware<crow::CORSHandler>();
//  cors.global();
//    .methods("POST"_method)
//    .origin("*");

  // Returns the Instruction that matches the given Id
  CROW_ROUTE(app, "/get_instruction_by_id")
    .methods(crow::HTTPMethod::POST)([](){
    // Retrieve the instruction Id
    // int instructionId = req.get_body_params("id");

    // Look in the SQL table for the given ingredient

    // Canned Data
    crow::json::wvalue::object instruction;
    instruction["step"] = "The quick brown fox jumped over the lazy dog";

    // Return the instruction
    return crow::json::wvalue(instruction);
  });

  // Returns the Ingredient that matches the given Id
  CROW_ROUTE(app, "/get_ingredient_by_id")
    .methods(crow::HTTPMethod::POST)([](){
    // Retrieve the ingredient Id
    // int ingredientId = req.get_body_params("id");

    // Look in the SQL table for the given ingredient

    // Canned Data
    crow::json::wvalue::object ingredient;
    ingredient["name"] = "apple";

    crow::json::wvalue::object amounts;
    amounts["amount"] = 1;
    amounts["unit"] = "each";
    ingredient["amounts"] = amounts;

    // Return the ingredient
    return crow::json::wvalue(ingredient);
  });

  // Returns the Recipe that matches the given Id
  CROW_ROUTE(app, "/get_recipe_by_id")
    .methods(crow::HTTPMethod::POST)([](){
    // Retrieve the recipe id
    // int recipeId = req.get_body_params("id");

    // Look in the SQL table for the given recipe

    // Canned Data
    crow::json::wvalue recipe("recipe");
    recipe["name"] = "Sample";

    // List of ingredients
    crow::json::wvalue::object ingredients;
    ingredients["1"] = "00123";
    ingredients["2"] = "00234";

    recipe["ingredients"] = ingredients;

    // List of instructions
    crow::json::wvalue::object instructions;
    instructions["1"] = "00345";
    instructions["2"] = "00456";

    recipe["instructions"] = instructions;

    // Respond with the Recipe in JSON format
    crow::response res (recipe);
    res.write(recipe.dump());
    return recipe;
  });

  // Create default Crow path
  CROW_CATCHALL_ROUTE(app)([](){
    return "<h1>Hello, World</h1>";
  });

  CROW_ROUTE(app, "/json")([](){
    crow::json::wvalue test = {{"Hello", "World"}};
    return test;
  });

  CROW_ROUTE(app, "/res")([](){
    crow::response res;
    res.body = "Hello, World";
    res.end();
    return res;
  });

  // Start the App on Port 8080 and this IP
  app.bindaddr("192.168.1.150")
     .port(8080)
     .multithreaded()
     .run();
}
