
function appendIngredient(ingredientId) {
  // Retrieve Ingredients DOM
  let ingredients_list = document.getElementById('ingredients');

  // Create ingredient-item
  let new_ingredient = document.createElement('ingredient-item');

  // Retrieve the Ingredient
  let ingredient;
  let data = fetch("http://192.168.1.150:8080/get_ingredient_by_id", {
      method: 'POST',
      body: JSON.stringify({
        'id':ingredientId,
      })
    }).then(response => {
      console.log(response);
      return response.json;
    }).then(data => {
      console.log(data);
      console.log(data.name);
      ingredient = data;
    });

  // Add attribute amount
  new_ingredient.setAttribute('amount', ingredient.amounts.amount);
  // Add attribute unit
  new_ingredient.setAttribute('unit', ingredient.amounts.unit);
  // Add attribute name
  new_ingredient.setAttribute('name', ingredient.name);
  // Add attribute usda_num
  // TODO: Add attribute processing
  // TODO: Add attribute substitutes
  // TODO: Add attribute notes

  // Append ingredient-item to Ingredients DOM
  ingredients_list.appendChild(new_ingredient);
}

function appendInstruction (instruction) {
  // Retrieve Instructions DOM
  let instructions_list = document.getElementById('instructions');

  // Create instruction-item
  let new_instruction = document.createElement('instruction-item');

  // new_instruction.innerHTML = instruction.step
  new_instruction.innerHTML = "Lorem Ipsum Dimsum.  The quick brown fox jumped over the lazy dog while calling him names as she trotted majestically into the woods for a snack";

  // TODO: add haccp
  // TODO: add notes

  // Append instruction-item to Instructions DOM
  instructions_list.appendChild(new_instruction);
}

function createRecipe() {
  // Retrieve the recipe_id from URL
  const params = new URLSearchParams(window.location.search);
  let recipeId = params.get("id");

  // Retrieve the Recipe
  let recipe;
  let armadilla = fetch("http://192.168.1.150:8080/get_recipe_by_id", {
      method: 'POST',
      body: JSON.stringify({
        'id':recipeId,
      })
    }).then(response => {
      console.log(response);
      return response.json;
    }).then(data => {
      console.log(data);
      recipe = data;
    });

  // getRecipeById(recipe_id)

//  for (let i = 0; i < recipe.ingredients.length; i++) {
    // Retrieve the list of ingredients
    appendIngredient();
//  }

  // for (let i = 0; i < data.ingredients.length; i++) {
  //  ingredient = getIngredientById(data.ingredients[i].ingredient_id)
  //  appendIngredient(ingredient)
  // }

  // Retrieve the list of instructions
  appendInstruction();

  // for (let i = 0; i < data.instructions.length; i++) {
  //   instruction = getInstructionById(data.instructions[i].instruction_id)
  //   appendInstruction(instruction)
  // }
}

createRecipe();
