class Ingredient extends HTMLElement {

  constructor() {
    super();

    // Create a shadow root
    const shadowRoot = this.attachShadow({mode: 'open'});

    // Apply external sytle to the shadow DOM
    const linkElem = document.createElement('link');
    linkElem.setAttribute('rel', 'stylesheet');
    linkElem.setAttribute('href', 'ingredient.css');
    shadowRoot.appendChild(linkElem);

    // Create the outer shell
    const wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'ingredient');

    // Populate the amounts
    const amounts = document.createElement('div');
    amounts.setAttribute('id', 'amounts');
    wrapper.appendChild(amounts);

    // Populate the amount
    const amount = document.createElement('p');
    amount.setAttribute('id', 'amount');
    if (this.hasAttribute('amount')) {
      amount.innerHTML = this.getAttribute('amount');
    }
    amounts.appendChild(amount);

    // Populate the unit
    const unit = document.createElement('p');
    unit.setAttribute('id', 'unit');
    if (this.hasAttribute('unit')) {
      unit.innerHTML = this.getAttribute('unit');
    }
    amounts.appendChild(unit);

    // Populate the name
    const name = document.createElement('p');
    name.setAttribute('id', 'name');
    if (this.hasAttribute('name')) {
      name.innerHTML = this.getAttribute('name');
      wrapper.appendChild(name);
    }

    // Populate the usda_num
    const usda_num = document.createElement('p');
    usda_num.setAttribute('id', 'usda_num');
    if (this.hasAttribute('usda_num')) {
      usda_num.innerHTML = this.getAttribute('usda_num');
    }
    wrapper.appendChild(usda_num);

    // Populate the processing
    const processing = document.createElement('p');
    processing.setAttribute('id', 'processing');
    if (this.hasAttribute('processing')) {
      processing.innerHTML = this.getAttribute('processing');
    }
    wrapper.appendChild(processing);

    // Populate the substitutions
    // TODO:
    // Populate the processing

    const notes = document.createElement('p');
    notes.setAttribute('id', 'notes');
    if (this.hasAttribute('notes')) {
      notes.innerHTML = this.getAttribute('notes');
    }
    wrapper.appendChild(notes);

    this.shadowRoot.appendChild(wrapper);
  }

  // TODO: Callbacks?
}

customElements.define("ingredient-item", Ingredient);
